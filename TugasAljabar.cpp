// TugasAljabar.cpp : This file contains the 'main' function. Program execution begins and ends there.
#include <numeric> // std::inner_product
#include <vector>
#include <cmath>
#include <iostream>
#include <complex>
using namespace std;

void menu();
void calcVector();
void findTransporse();
void findSquareMatrix();

int main()
{
    menu();
   
    system("pause");
}



void menu() {

    int pilihan;
    char ulang;

    do {
        cout << "##  Program Sifat Aritmatika dan Norma Vektor  ##" << endl;
        cout << "=================================================" << endl;
        system("Color 0A");

        cout << "Copyright @2021" << endl;
        cout << "Create by Rahman Suwito & Sigiwimala Palwiantaka" << endl;
        system("Color 0B");

        cout << endl;
        cout << "Pilih Opsi Program" << endl;
        cout << "1. Norma Vektor" << endl;
        cout << "2. Cari Transpose Matrix" << endl;
        cout << "3. Matriks persegi" << endl;
        cout << "0. Keluar" << endl;
        cout << endl;

        cout << "Pilihan anda: ";
        cin >> pilihan;


        switch (pilihan) {
        case 1:
            calcVector();
          
            break;
        case 2:
            findTransporse();
            break;

        case 3:
            findSquareMatrix();

            break;

        case 0:

            cout << "Terima kasih" << endl;
            system("pause");
            cout << endl;
            cout << endl;
            exit(3);

            break;
        default:
            cout << "Menu tidak tersedia" << endl;
        }
        cout << endl;
        system("pause");

        cout << "Ingin memilih menu lain (y/t)? ";
        cin >> ulang;
        cout << endl;


    } while (ulang != 't');

    cout << "Terima kasih...";

    cout << endl;

}




void calcVector() {
    int x = 0;
    int y = 0;
    complex <double> cn(x, y);
    cout << "##  Program Sifat Aritmatika dan Norma Vektor  ##" << endl;
    cout << "=================================================" << endl;
    system("Color 0A");
    cout << "##  please Input value element x, y" << endl;
    
    if (x == 0 && y == 0) {
        cout << "Input elemen x :" << endl;
        cin >> x;
        cout << "Value element x adalah " << x << endl;
        cout << "Input elemen y : " << endl;
        cin >> y;
        cout << "Value element y adalah " << y << endl;



        cout << "Panjang Vektor dari (" << 3 << "," << 4 << ") adalah ";
        complex <double> cn(x, y);
        cout << sqrt(norm(cn)) << endl;

      
    }

}


void findTransporse() {
    
    int a[10][10], transpose[10][10], row, column, i, j;

    cout << "Enter rows and columns of matrix: " << endl;
    cin >> row >> column;
    cout << "\nEnter elements of matrix: " << endl;

    // Storing matrix elements
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < column; ++j) {
            cout << "Enter element a" << i + 1 << j + 1 << ": ";
            cin >> a[i][j];
        }
    }

    // Printing the a matrix
    cout << "\nEntered Matrix: " << endl;
    for (int i = 0; i < row; ++i) {
        for (int j = 0; j < column; ++j) {
            cout << " " << a[i][j];
            if (j == column - 1)
                cout << endl << endl;
        }
    }

    // Computing transpose of the matrix
    for (int i = 0; i < row; ++i)
        for (int j = 0; j < column; ++j) {
            transpose[j][i] = a[i][j];
        }

    // Printing the transpose
    cout << "\nTranspose of Matrix: " << endl;
    for (int i = 0; i < column; ++i)
        for (int j = 0; j < row; ++j) {
            cout << " " << transpose[i][j];
            if (j == row - 1)
                cout << endl << endl;
        }



}


void findSquareMatrix() {
   
    int m, n, i, j, A[10][10];
    cout << "Enter the number of rows and columns of the matrix : " << endl;
    cin >> m >> n;
    cout << "Enter the array elements : " << endl;
    for (i = 0; i < m; i++)
        for (j = 0; j < n; j++)
            cin >> A[i][j];
    cout << "Matrix : \n ";
    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
            cout << A[i][j] << "  ";
        cout << "\n ";
    }
    if (m == n)
        cout << "The entered array is a square matrix." << endl;
    else
    {
        cout << "The entered array is not a square matrix." << endl;
        exit(0);
    }
    cout << "The diagonal elements are : \n";
    for (i = 0; i < m; i++)
    {
        for (j = 0; j < n; j++)
        {
            if (i == j)
                cout << A[i][j];
            else
                cout << "  ";
        }
        cout << "\n";
    }
}




